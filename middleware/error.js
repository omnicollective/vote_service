const ErrorResponse = require("../utils/errorResponse");

const errorHandler = (err, req, res, next) => {

    let error = { ...err }
    error.message = err.message;

    // Mongoose bad Object Id
    if (err.name === 'CastError') {
        const message = `Resource not found`;
        error = new ErrorResponse(message, 404);
    }

    // Mongoose duplicate key
    if(err.code === 11000) {
        const message = `Duplicate field value entered`;
        error = new ErrorResponse(message, 400);
    }

    // Mongoose validator error
    if (err.name === 'ValidationError') {
        console.log(err);
        const message = Object.values(err.errors).map(val => val.message);
        error = new ErrorResponse(message, 400);
    }

    console.log(err.name);
    res.status(error.statusCode || 500).json({
        success: false,
        error: error.message || 'Server Error'
    }); 
}

module.exports = errorHandler;