const path = require('path');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Vote = require("../models/Vote");
const User = require("../models/User");

// @desc    Get all votes, send all reqs for data here
// @route   GET /api/vote-service/v1/votes
// @access  Public
exports.getVotes = asyncHandler(async (req, res, next) => {
  res
    .status(200)
    .json(res.advancedResults);
});

// @desc    Get single vote
// @route   GET /api/vote-service/v1/votes/:id
// @access  Public
exports.getVote = asyncHandler(async (req, res, next) => {
  const vote = await Vote.findById(req.params.id);

  if (!vote) {
    return next(
      new ErrorResponse(`Vote not found with id of ${req.params.id}`, 404)
    );
  }

  res.status(200).json({
    success: true,
    data: vote,
  });

});

// @desc    Create new vote
// @route   POST /api/vote-service/v1/votes
// @access  Private
exports.createVote = asyncHandler(async (req, res, next) => {
  req.body.userID = req.user.id
  if (!req.user.id) {
    return next(
      new ErrorResponse(`Must be a user to vote`, 400)
    );
  }
  const publishedVote = await Vote.findOne({ userID: req.user.id, itemID: req.body.itemID });
  if (publishedVote) {
    return next(
      new ErrorResponse(`The user with ID ${req.user.id} has already voted on this item`, 400)
    );
  }
  const newVote = await Vote.create(req.body);
  res.status(201).json({
    success: true,
    data: newVote,
  });
});

// @desc    Update vote
// @route   PUT /api/vote-service/v1/votes/:id
// @access  Private
exports.updateVote = asyncHandler(async (req, res, next) => {
  let vote = await Vote.findById(req.params.id);

  if (!vote) {
    return next(
      new ErrorResponse(`Collective not found with id of ${req.params.id}`, 404)
    );
  }

  // check if creator and updater are the same user
  if (vote.userID.toString() !== req.user.id) {
    return next(
      new ErrorResponse(`User ${req.user.id} is not authorized to update this vote`, 401)
    )
  }

  vote = await Vote.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  res.status(200).json({
    success: true,
    data: vote
  });

});

// @desc    Delete vote
// @route   DELETE /api/vote-service/v1/votes/:id
// @access  Private
exports.deleteVote = asyncHandler(async (req, res, next) => {

  const vote = await Vote.findById(req.params.id);

  if (!vote) {
    return next(
      new ErrorResponse(`Vote not found with id of ${req.params.id}`, 404)
    );
  }

  // check if creator and updater are the same user
  if (vote.userID.toString() !== req.user.id) {
    return next(
      new ErrorResponse(`User ${req.user.id} is not authorized to delete this vote`, 401)
    )
  }

  vote.remove();

  res.status(200).json({ success: true, data: {} });
});