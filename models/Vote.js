const mongoose = require("mongoose");
const slugify = require("slugify");

const VoteSchema = new mongoose.Schema(
  {
    itemID: {
      type: String,
      required: true,
    },
    isPost: {
      type: Boolean,
      required: true,
    },
    tag: {
      type: [String],
      enum: ["Positive", "Negative", "Neutral"]
    },
    createdAt: {
      type: Date,
      default: Date.now,
    },
    userID: {
      type: String,
      required: true,
    },
  },
  {
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

module.exports = mongoose.model("Vote", VoteSchema);
